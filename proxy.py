import string
import threading
import socket
import random
import Queue
import openflow
import security
import sys
import os
import json

SHARED_KEY_LENGTH = 32 #bytes
ENCRYPT_SWITCH_CONTROLLER = True
ENCRYPT_CONTROLLER_SWITCH = True
class OpenflowProxy:
   _ofsecurity = None
   _shared_key = None
   _shared_key_length = SHARED_KEY_LENGTH #bytes
   _can_encrypt = False
   _proxy_id = ""
   _mode = ""
   _conn1 = None #connection to switch
   _conn2 = None #connection to controller
   _conn_close = False
   _peer_keyhash = None #other host key
   _outqueue = Queue.Queue() #queue for pkt before encryption established
   _run_evt = None
   
   _threads = []
   def __init__(self,mode,run_evt,conn,ofsecurity,peer_keyhash=None):
      self._mode = mode
      self._ofsecurity = ofsecurity
      self._peer_keyhash = peer_keyhash
      self._proxy_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
      self._run_evt = run_evt
      self._conn1 = conn

   def _connect_to_controller(self,controller_ip,controller_port):
      try:
         self._conn2 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
         self._conn2.connect((controller_ip,controller_port))
         return True
      except Exception as e:
         print "[connect_to_controller] %s"%e
         return False
   
   def start_proxy(self,controller_ip,controller_port):
      #print "proxy %s started..."%self._proxy_id
      connected = self._connect_to_controller(controller_ip,controller_port)
      if connected==False:return False
      try:
         t1 = threading.Thread(target=self._recv_switch)
         t2 = threading.Thread(target=self._recv_controller)
         self._threads = [t1,t2]
         t1.start()
         t2.start()
         t_guard = threading.Thread(target=self.join)
         t_guard.start()
         return True
      except Exception as e:
         print "[start_proxy] %s"%e
         return False

   def join(self):
      for p in self._threads: p.join()
      #print "proxy %s completed..."%self._proxy_id

   def _recv_switch(self):
      self._recv(self._process_switch,self._conn1,self._conn2)

   def _recv_controller(self):
      self._recv(self._process_controller,self._conn2,self._conn1)

   def _recv(self,processor,conn1,conn2):
      try:
         received = ""
         while self._run_evt.is_set() and self._conn_close==False:
            r = conn1.recv(1)
            if r is None: break
            if len(r)==0:continue
            received+=r
            if len(received)<8:continue
            of_length = ((ord(received[2])<<8)|ord(received[3]))&0xffff
            if len(received)<of_length:continue
            of_message = received[:of_length]
            received = received[of_length:]
            of_messages = processor(of_message)
            if of_messages is None:break
            for ofm in of_messages:
               conn2.sendall(ofm)
         self._conn_close=True
      except Exception as e:
         self._conn_close=True
         print "[_recv] %s"%e
 
   def _process_switch(self,of_message):
      try:
         if self._mode=="switch":
            return self._process_switch_switch_mode(of_message)
         elif self._mode=="controller":
            return self._process_switch_controller_mode(of_message)
         else: return [of_message]
      except Exception as e:
         print "[_process_switch] %s"%e

   # process message from the switcch on the controller-side proxy
   def _process_switch_controller_mode(self,of_message):
      # mode controller
      # -> determine security config
      # -> decrypt_message
      header = openflow.of_header(of_message)
      if header['type'] == 0x0: #hello packet
         print "received hello from switch"
         hello,sec_ele = openflow.hello_strip_sec(of_message)
         if sec_ele is not None and self._peer_keyhash is None:
            if 'payload' in sec_ele:
               sec_auth = sec_ele['payload']
               (verified,params) = ofs.verify_auth(sec_auth)
               print "%s:%s"%(verified,params)
               if verified: 
                  print "[CLIENT AUTHENTICATED, START ENCRYPTION]"
                  self._shared_key = params[2]
                  self._peer_keyhash = params[1]
                  self._can_encrypt = True
                  self._controller_ack_security()
         return [hello]
      elif header['type']==0x5f: #encrypted openflow packet
         if self._peer_keyhash is None: return None
         print "received encrypted message from switch"
         # encryption not configured on connection yet
         if self._can_encrypt==False: return []
         if len(self._shared_key)==0: return []
         ofm = ofs.aes_decrypt(header['payload'],self._shared_key)
         if ofm is None: return []
         return [ofm]
      else:
         # controller proxy allow of_message to be forwarded to controller
         # if connection does not support authentication/encryption yet
         if self._can_encrypt and ENCRYPT_SWITCH_CONTROLLER: 
            print "[WARNING] dropped plaintext message from controller"
            return []
         else: 
            print "received plaintext message from switch"
            return [of_message]
      return None

   # process message from the switch on the switch-side proxy
   def _process_switch_switch_mode(self,of_message):
      # mode switch
      # -> append security config in hello
      # -> encrypt_message
      header = openflow.of_header(of_message)
      if header['type'] == 0x0: #hello packet
         if self._can_encrypt: return [] #auth already established
         print "sending hello to controller"
         self._shared_key = ofs.random(self._shared_key_length)
         sec_auth = self._ofsecurity.gen_auth(self._shared_key,self._peer_keyhash)
         if sec_auth is None:return None
         of_message = openflow.hello_add_sec(of_message,sec_auth)
         # if the controller does not support security, 
         # it will disconnect. thats good :) if it doesn't disconnected
         # we wait for a ack before transmitting the 
         # rest of the packet with encryption
         return [of_message]
      else:
         if ENCRYPT_SWITCH_CONTROLLER==False: 
            print "sending plaintext message to controller"
            return [of_message]
         # dont send any packet when proxy cannot encrypt
         # put in queue, will send when encryption enabled
         if self._can_encrypt==False: 
            self._outqueue.put(of_message)
            return []
         of_messages = []
         print "sending encrypted message to controller"
         while self._outqueue.empty()==False:
            ofm = None
            try:
               ofm = self._outqueue.get(False)
            except:
               pass
            if ofm is None: 
               print "err1"
               continue
            enc_ofm = self._encrypt_ofm(ofm)
            if enc_ofm is None: 
               print "err2"
               continue
            of_messages.append(enc_ofm)
         enc_ofm = self._encrypt_ofm(of_message)
         if enc_ofm is not None: of_messages.append(enc_ofm)
         return of_messages
      return None

   def _process_controller(self,of_message):
      try:
         if self._mode=="switch":
            return self._process_controller_switch_mode(of_message)
         elif self._mode=="controller":
            return self._process_controller_controller_mode(of_message)
         else: return [of_message]
      except Exception as e:
         print "[_process_controller] %s"%e

   # process message from the controller on the switch-side proxy
   def _process_controller_switch_mode(self,of_message):
      # mode switch
      # -> receive controller security ack
      # -> decrypt message
      header = openflow.of_header(of_message)
      if header['type'] == 0x5e: #controller ack security
         sec_auth = header['payload']
         verified,para = ofs.verify_auth(sec_auth)
         if verified:
            print "[SERVER AUTHENTICATED, START ENCRYPTION]"
            self._can_encrypt = True
         return []
      elif header['type']==0x5f: #encrypted openflow packet
         # encryption not configured on connection yet, 
         # should not receive encrypted message
         if self._can_encrypt==False: return []
         print "receieved encrypted message from controller"
         ofm = ofs.aes_decrypt(header['payload'],self._shared_key)
         if ofm is None: return []
         return [ofm]
      else:
         # if encryption is enabled, drop non-encrypted messages
         if self._can_encrypt and ENCRYPT_CONTROLLER_SWITCH: 
            print "[WARNING] dropped plaintext message from controller"
            return [] 
         print "received plaintext message from controller"
         return [of_message]
      return None

   # process message from the controller on the controller-side proxy
   def _process_controller_controller_mode(self,of_message):
      header = openflow.of_header(of_message)
      # mode switch
      # -> decrypt of_message
      if self._can_encrypt and ENCRYPT_CONTROLLER_SWITCH:
            print "sending encrypted message to switch"
            enc_ofm = self._encrypt_ofm(of_message)
            if enc_ofm is None:return []
            return [enc_ofm]
      else: 
            print "sending plaintext message to switch"
            return [of_message]
      return None

   def _encrypt_ofm(self,ofm):
      try:
         ct = ofs.aes_encrypt(ofm,self._shared_key)
         if ct is None: return None
         of_msglen = len(ct)+8
         len_b1 = chr((of_msglen>>8)&0xff)
         len_b2 = chr(of_msglen&0xff)
         # type 0x5f: encrypted openflow message
         enc_ofm = "\x04\x5f"+len_b1+len_b2+"\x00\x00\x00\x00"+ct
         return enc_ofm
      except Exception as e:
         print "[_encrypt_ofm] %s"%e
         return None

   def _controller_ack_security(self):
      try:
         nonce = ofs.random(16)
         sec_auth = ofs.gen_auth(nonce,self._peer_keyhash)
         if sec_auth is None: return
         of_msglen = len(sec_auth)+8
         len_b1 = chr((of_msglen>>8)&0xff)
         len_b2 = chr(of_msglen&0xff)
         ack_pkt = "\x04\x5e"+len_b1+len_b2+"\x00\x00\x00\x00"+sec_auth
         self._conn1.sendall(ack_pkt)
      except Exception as e:
         print "[_controller_ack_security] %s"%e

def help_exit():
   print "python proxy.py <mode: switch/controller> <config_file>"
   exit()

if len(sys.argv)!=2:help_exit()
config_file = sys.argv[1]
if os.path.exists(config_file)==False:
   print "config file not found"
   help_exit()
config_f = open(config_file,"rb")
config_str = config_f.read()
config_f.close()
config_obj = json.loads(config_str)
controller_ip = ""
controller_port = ""
mode = config_obj['mode']
if mode not in ["switch","controller"]:
   print "Invalid mode"
   help_exit()
if os.path.exists(config_obj["my_key_path"])==False:
   print "Invalid my_key_path"
   help_exit()
for i in config_obj["peer_key_path"]:
   if os.path.exists(i)==False:
      print "Invalid peer_key_path: %d"%i
      help_exit()
ofs = security.OFSecurity(SHARED_KEY_LENGTH,config_obj["my_key_path"])
for i in config_obj["peer_key_path"]: ofs.add_peer_keys(i)

controller_key_path = ""
switch_key_path = ""
if mode=="switch":
   controller_key_path = "controller_pub.pem"
   switch_key_path = "switch_priv.pem"
elif mode=="controller":
   controller_key_path = "controller_priv.pem"
   switch_key_path = "switch_pub.pem"

listen = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
listen.bind((config_obj["listen_ip"],config_obj["listen_port"]))
listen.listen(8)
run_evt = threading.Event()
run_evt.set()

c_k = None
if mode=="switch": c_k=ofs.get_peer_key(0)
proxies = []
print "Starting proxy..."
try:
   while True:
      conn,addr = listen.accept()
      print "Connection from:",addr
      proxy = None
      if mode=="switch":proxy=OpenflowProxy(mode,run_evt,conn,ofs,c_k)
      elif mode=="controller":proxy=OpenflowProxy(mode,run_evt,conn,ofs,None)
      ok = proxy.start_proxy(config_obj["controller_ip"],config_obj["controller_port"])
      if ok==True: proxies.append(proxy)
      else: conn.close()
except KeyboardInterrupt:
   print "Terminating proxy..."
   run_evt.clear()
   for p in proxies: p.join()
