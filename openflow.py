def of_header(data):
   if len(data)<8:return None
   d = []
   for i in range(8): d.append(ord(data[i]))
   of_version = d[0]&0xff
   of_type = d[1]&0xff
   of_length = ((d[2]<<8)|d[3])&0xffff
   of_xid = ((d[4]<<24)|(d[5]<<16)|(d[6]<<8)|d[7])&0xffffffff
   m = {"version":of_version,"type":of_type,"length":of_length,"xid":of_xid,"payload":data[8:]}
   return m

def of_hello(data):
   hello_elements = []
   i = 0
   while i<len(data):
      ele_type    = ((ord(data[i+0])<<8)|ord(data[i+1]))&0xffff
      ele_length = ((ord(data[i+2])<<8)|ord(data[i+3]))&0xffff
      ele_payload = data[i+4:i+ele_length]
      i = i+ele_length
      e = {"type":ele_type,"length":ele_length,"payload":ele_payload}
      hello_elements.append(e)
   return hello_elements

def hello_add_sec(hello_data,sec_ele):
   hdr = of_header(hello_data)
   hello_elements = of_hello(hdr['payload'])
   sec_payload = []
   for i in range(len(sec_ele)):sec_payload.append((sec_ele[i]))
   sec_tlv = {"type":0x54,"length":len(sec_ele)+4,"payload":sec_payload}
   hello_elements.append(sec_tlv)
   hello = bytearray(build_hello(hdr,hello_elements))
   return hello

def hello_strip_sec(hello_data):
   hdr = of_header(hello_data)
   hello_elements = of_hello(hdr['payload'])
   strip_elements = []
   sec_ele = None
   for e in hello_elements:
      if e['type']==0x54: sec_ele=e
      else: strip_elements.append(e)
   hello = bytearray(build_hello(hdr,strip_elements))
   return (hello,sec_ele)

def build_header(hdr):
   h = []
   h = h+int_to_8bit(hdr['version'])
   h = h+int_to_8bit(hdr['type'])
   h = h+int_to_16bit(hdr['length'])
   h = h+int_to_32bit(hdr['xid'])
   return h

def build_hello(hdr,elements):
   payload = []
   for ele in elements:
      p = int_to_16bit(ele['type'])
      p = p + int_to_16bit(ele['length'])
      d = []
      for i in ele['payload']:d.append(i)
      p = p + d
      payload = payload+p
   hdr['length'] = len(payload)+8
   header = build_header(hdr)
   new_hello = header +payload
   return new_hello

def int_to_8bit(i):
   return [i&0xff]

def int_to_16bit(i):
   b1 = i&0xff
   b2 = (i>>8)&0xff
   return [b2,b1]

def int_to_32bit(i):
   b1 = i&0xff
   b2 = (i>>8)&0xff
   b3 = (i>>16)&0xff
   b4 = (i>>24)&0xff
   return [b4,b3,b2,b1]

