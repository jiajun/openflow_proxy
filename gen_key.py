from Crypto.PublicKey import RSA
import sys

key_length  = 4096
key_name = ""
if len(sys.argv)>1: key_name = sys.argv[1].strip()
if len(key_name)==0:
   print "key name not sepcified"
   print "%s <key name> [key length]"%sys.argv[0]
   exit()

if len(sys.argv)>2: key_length = int(sys.argv[2])
key = RSA.generate(key_length)
pubkey = key.publickey()
f = open('%s_priv.pem'%key_name,'w')
f.write(key.exportKey('PEM'))
f.close()
f = open('%s_pub.pem'%key_name,'w')
f.write(pubkey.exportKey('PEM'))
f.close()
