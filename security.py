import os
import time
import struct
from Crypto.Hash import HMAC,SHA
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES,PKCS1_OAEP
from Crypto.Signature import PKCS1_PSS
from Crypto.Random import get_random_bytes
class OFSecurity:

   _shared_key = None
   _shared_key_length = 0
   _allowed_timeskew = 5 #seconds
   _my_rsa_key = None
   _my_rsa_keyhash = ""
   _peer_rsa_key_str = {}
   _peer_rsa = {}
   _peer_keyhash = {}

   def __init__(self,key_length,my_key_path):
      self._shared_key_length = key_length
      my_key_str = self.read_file(my_key_path)
      if my_key_str is not None:
         if len(my_key_str)>0:
               rsa = RSA.importKey(my_key_str)
               pub_key = rsa.publickey()
               pub_key_str = pub_key.exportKey()
               keyhash = self.get_key_hash(pub_key_str)
               self._my_rsa_key = rsa
               self._my_rsa_keyhash = keyhash
      return
  
   def add_peer_keys(self,key_path):
      try:
         keyhash = None
         keyhash_hex = None
         rsa = None
         key_str = self.read_file(key_path)
         if key_str is not None: 
            if len(key_str)>0:
               rsa = RSA.importKey(key_str)
               pub_key = rsa.publickey()
               pub_key_str = pub_key.exportKey()
               keyhash = self.get_key_hash(pub_key_str)
               keyhash_hex = self.tostring(keyhash)
               self._peer_rsa_key_str[keyhash_hex] = key_str
               self._peer_rsa[keyhash_hex] = rsa
               self._peer_keyhash[keyhash_hex] = keyhash
         return keyhash_hex
      except Exception as e:
         print "[OFSsecurity:add_keys] %s"%e
         return None

   def get_peer_key(self,idx):
      count = 0
      for i in self._peer_keyhash:
         if count==idx:return i
         count+=1
      return None

   def random(self,length):
      return get_random_bytes(length)

   def read_file(self,path):
      if path is None: return None
      if os.path.exists(path)==False: return None
      try:
          f = open(path,"rb")
          c = f.read().strip()
          f.close()
          return c
      except Exception as e:
          print "[OFSsecurity:read_file] %s"%e
          return None

   def tostring(self,hexbytes):
      s = ""
      for i in range(len(hexbytes)):s+="%02x"%ord(hexbytes[i])
      return s

   def get_timestamp(self):
      timestamp = int(time.time()) #seconds since epoch
      b = struct.pack(">i",timestamp)
      return b
   
   def int_to_word(self,i):
      b1 = (i>>8)&0xff
      b2 = i&0xff
      return chr(b1)+chr(b2)

   def get_key_hash(self,k):
      try:
         sha = SHA.new()
         sha.update(k)
         h = sha.digest()
         return h
      except Exception as e:
         print "[OFSsecurity:get_key_hash] %s"%e
         return ""

   def aes_encrypt(self,data,key):
      # sign-then-encrypt
      try:
         print "EN: %s [%d]"%(self.tostring(data[:8]),len(data))
         data_len = len(data)
         data_len_b = self.int_to_word(data_len)
         pad_len = AES.block_size-((data_len+2+SHA.digest_size)%AES.block_size)
         pad = ""
         for i in range(pad_len):pad+="\x00"
         h = HMAC.new(key,digestmod=SHA.new())
         h.update(data+pad)
         digest = h.digest()
         iv = self.random(AES.block_size)
         cipher = AES.new(key,AES.MODE_CBC,iv)
         ct = cipher.encrypt(data_len_b+digest+data+pad)
         msg = iv+ct
         return msg
      except Exception as e:
         print "[OFSecurity:aes_encrypt] %s"%e
         return None

   def aes_decrypt(self,msg,key):
      try:
         iv = msg[:AES.block_size]
         ct = msg[AES.block_size:]
         cipher = AES.new(key,AES.MODE_CBC,iv)
         pt = cipher.decrypt(ct)
         data_len = (((ord(pt[0])&0xff)<<8)|ord(pt[1]))&0xffff
         digest = pt[2:2+SHA.digest_size]
         data = pt[2+SHA.digest_size:]
         h = HMAC.new(key,digestmod=SHA.new())
         h.update(data)
         if digest != h.digest():
            print "HMAC failed"
            return None
         data = data[:data_len]
         print "DE: %s [%d]"%(self.tostring(data[:8]),len(data))
         return data
      except Exception as e:
         print "[OFSecurity:aes_decrypt] %s"%e
         return None
     
   def gen_auth(self,k,encrypt_keyhash):
      r = None
      if encrypt_keyhash is None: return r
      if encrypt_keyhash not in self._peer_rsa: return r
      encrypt_rsa = self._peer_rsa[encrypt_keyhash]
      sign_rsa = self._my_rsa_key
      if encrypt_rsa.can_encrypt()==False:return r
      if sign_rsa.has_private()==False: return r
      if sign_rsa.can_sign()==False: return r
      try:
         timestamp = self.get_timestamp()
         keyhash = self._my_rsa_keyhash
         pt = timestamp+keyhash+k
         sha = SHA.new()
         sha.update(pt)
         signer = PKCS1_PSS.new(sign_rsa)
         signature = signer.sign(sha)
         cipher = PKCS1_OAEP.new(encrypt_rsa)
         ct = cipher.encrypt(pt)
         l1 = self.int_to_word(len(ct))
         l2 = self.int_to_word(len(signature))
         auth = l1+l2+ct +signature
         return auth
      except Exception as e:
         print "[OFSsecurity:gen_sec_ele] %s"%e
         return None
      
   def verify_auth(self,sec_auth):
      r = (False,None)
      decrypt_rsa = self._my_rsa_key
      if decrypt_rsa.has_private()==False: return r
      try:
         if len(sec_auth)<self._shared_key_length:return r
         l1 = ((ord(sec_auth[0])<<8)|ord(sec_auth[1]))&0xffff
         l2 = ((ord(sec_auth[2])<<8)|ord(sec_auth[3]))&0xffff
         if type(sec_auth[0])==int:
            e = ""
            for i in sec_auth: e+=chr(i)
            sec_auth=e
         ct = sec_auth[4:4+l1]
         cipher = PKCS1_OAEP.new(decrypt_rsa)
         pt = cipher.decrypt(ct)
         signature = sec_auth[4+l1:]
         if len(signature)!=l2:return r
         if len(pt)==0:return r
         if len(signature)==0:return r
         params = self.get_auth_param(pt)
         if params is None:return r
         if len(params)!=3:return r
         if params[1] is None: return r
         verify_keyhash = params[1]
         if verify_keyhash not in self._peer_rsa: return r
         verify_rsa = self._peer_rsa[verify_keyhash]
         sha = SHA.new()
         sha.update(pt)
         verifier = PKCS1_PSS.new(verify_rsa)
         if verifier.verify(sha,signature)==False: return r
         timestamp = params[0]
         if abs(time.time()-timestamp)>self._allowed_timeskew: 
            print "WARNING: timeskew exceeded limit"
            return r
         return (True,params)
      except Exception as e:
         print "[OFSsecurity:verify_auth] %s"%e
         return r

   def get_auth_param(self,ele_pt):
      if len(ele_pt)<24:return None
      try:
         timestamp = struct.unpack(">i",ele_pt[0:4])[0]
         keyhash = ele_pt[4:24]
         if len(keyhash)==0:return None
         keyhash = self.tostring(keyhash)
         shared_key = ele_pt[24:]
         if len(shared_key)==0:return None
         return (timestamp,keyhash,shared_key)
      except Exception as e:
         print "[OFSsecurity:get_auth_param] %s"%e
         return None

      
      
